" https://www.2n.pl/blog/how-to-write-neovim-plugins-in-lua

" in plugin/combustion.vim
if exists('g:loaded_combustion') | finish | endif " prevent loading file twice

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

" command to run our plugin
" TODO: call run plugin with arguments like:
" Combustion:repl("chibi-scheme -R")
" Or Combustion:repl("guile")

command! combustion lua require'combustion'.combustion()

let &cpo = s:save_cpo " and restore after
unlet s:save_cpo

let g:loaded_combustion = 1
